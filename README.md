# static-on-stack

Safely wrap the promotion of a short-lived reference to a `'static` reference, under the
condition that it is passed to a function that never terminates.

See [`promote_to_static()`] for both how to use it and why it is assumed to be sound.
