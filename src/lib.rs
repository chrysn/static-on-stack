//! Safely wrap the promotion of a short-lived reference to a `'static` reference, under the
//! condition that it is passed to a function that never terminates.
//!
//! See [`promote_to_static()`] for both how to use it and why it is assumed to be sound.
#![no_std]

/// Execute the function `f`, and pass it `&T` promoted to be a `&'static T`.
///
/// ## Panics
///
/// The function `f` must not panic -- or, more precisely, the user must not expect panics from `f`
/// to be catchable. If a panic passes through this function, it will turn it into an abort, or
/// (if Rust's rules change in unforeseen ways) into an endless loop.
///
/// ## How this is sound
///
/// The precondition for this to be sound is that this function not only never terminates, but that
/// any panic flying out of the function causes an immediate abort of the program. A drop guard is
/// in place around the function's execution that makes any panic a double panic.
///
/// As [per the Drop documentation](https://doc.rust-lang.org/nightly/core/ops/trait.Drop.html) a
/// double panic "will likely abort the program" (i.e. it is not guaranteed), an extra panic guard
/// is in place that runs an infinite loop on drop. That is not very pretty, but it will only even
/// make it into optimized code if there is any way in which the double panic does *not* cause an
/// abort, in which case it does serve its critical role of ensuring that the lifetime of the
/// original argument still does not end.
pub fn promote_to_static<T: 'static>(local: &T, f: impl FnOnce(&'static T) -> Never) -> ! {
    struct DropGuard;
    impl Drop for DropGuard {
        fn drop(&mut self) {
            panic!("Must not unwind out of a function that promotes statics");
        }
    }
    struct WorstCaseDropGuard;
    impl Drop for WorstCaseDropGuard {
        fn drop(&mut self) {
            // It'd be tempting to call out to an extern function that is known not to exist, but
            // without optimizations enabled, this drop will stay in the code even though it is
            // only executed after an abort.
            loop {}
        }
    }
    let _worst_case_drop_guard = WorstCaseDropGuard;
    let _guard = DropGuard;
    // unsafe: See module level documentation
    f(unsafe { core::mem::transmute(local) })
}


#[doc(hidden)]
pub trait ReturnTypeExtractor {
    type ReturnType;
}
impl<T> ReturnTypeExtractor for fn() -> T {
    type ReturnType = T;
}
/// An alias for the `!` type, accessed through trait trickery originally described [by
/// SkiFire13](https://github.com/rust-lang/rust/issues/43301#issuecomment-912390203). It is
/// present only to allow expressing a `FnOnce(...) -> !` in the argument requirement of
/// [`promote_to_static`].
pub type Never = <fn() -> ! as ReturnTypeExtractor>::ReturnType;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    // This would need a #[should_abort to be actually useful :-/
    fn it_works() {
        let a = 42;
        promote_to_static(&a, |&a| panic!("Don't know what to do with {a}"));
    }
}
